#importing required packages 

import pyodbc
import datetime
import time
import os
import logging
from configparser import ConfigParser
import subprocess
from os import path
import sys
from subprocess import Popen
import shutil

def dbimportFunc():
    #create an instance of ConfigParser class.
    parser = ConfigParser()

    # read and parse the configuration file.
    parser.read("config_file.ini")

    dateTime = time.strftime('%Y%m%d')

    #Logging
    #Create and configure logger 
    if not os.path.exists("Logs"):
        os.mkdir("Logs")
    logging.basicConfig(filename="Logs\Logs_"+dateTime+".log", 
                        format='%(asctime)s %(message)s', 
                        level=logging.INFO,
                        filemode='a+') 
        
    #Creating an object 
    logger=logging.getLogger() 
    logger.info("########################### Database Import ################################\n") 


    #  defining the input values from config file
    config_dbServer = parser.get('config', 'dbServer')
    config_dbName = parser.get('config', 'dbName')
    config_dbUid = parser.get('config', 'dbUid')
    config_dbPwd = parser.get('config', 'dbPwd')
    config_backupFile = parser.get('config', 'dbbackupFile')
    config_datafiles = parser.get('config', 'datafiles')

    #Database backup File availability check:
    print("checking for backup file ....")
    dbFile_Exists = path.exists(config_backupFile) 


    if dbFile_Exists == True:
        print("Database backup is available")
        logger.info("Database backup is available")
    else:
        print("Backup file is not available in the specified path")
        logger.info("Database backup is not available")
        sys.exit(0)

    # SQL Connection details
    """ The words "try" and "except" are Python keywords and are used to catch exceptions.*//
    The code within the try clause will be executed statement by statement.

    If an exception occurs, the rest of the try block will be skipped and the
    except clause will be executed."""

    try:
        connection = pyodbc.connect(driver='{SQL Server Native Client 11.0}',
                            server=config_dbServer,
                            # database='Windchilldb',
                            UID=config_dbUid,
                            PWD=config_dbPwd,
                            trusted_connection='yes',
                            autocommit=True)

        #Creating Connection
        cursor = connection.cursor()


        if cursor:
            print("Connection Established")
            logger.info("SQL Connection Established") 
        else:
            print("Error occurred in SQL connection. Check SQL connection parameters")
            logger.info("Error occurred in SQL connection. Check SQL connection parameters")
            sys.exit(0)
     
    except Exception as e:
        print(e)
        logger.error(e) 
        sys.exit(0)

    # Database drop incase if exists
    try:
        cursor.execute("DECLARE @kill varchar(8000) = '';   SELECT @kill = @kill + 'kill ' + CONVERT(varchar(5), session_id) + ';' FROM sys.dm_exec_sessions WHERE database_id  = db_id('windchilldb') EXEC(@kill)")
        cursor.execute("DROP DATABASE Windchilldb")
    except:
        pass
    
    #Removing old database files
    for filename in os.listdir(config_datafiles):
        filepath = os.path.join(config_datafiles, filename)
        try:
            shutil.rmtree(filepath)
            logging.info("Files under "+config_datafiles+" has been removed")
            print("Files under "+config_datafiles+" has been removed")
        except OSError:
            os.remove(filepath)
            logging.info("Files under "+config_datafiles+" has been removed")
            print("Files under "+config_datafiles+" has been removed")

    # Wait for 3 seconds
    time.sleep(3)
    restore_output = subprocess.Popen('sqlcmd -S localhost  -U {0} -P {1} -Q "RESTORE DATABASE {2} FROM DISK = "{3}"" '.format(config_dbUid,config_dbPwd,config_dbName,config_backupFile),stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (stdout, stderr) = restore_output.communicate()


    if restore_output.returncode != 0:
        logging.error("Exception : "+ str(stderr))
        sys.exit(0)
    else:
        if "RESTORE DATABASE successfully processed" in str(stdout):
            print("Restore Successfull \n"+ str(stdout))
            logging.info(stdout)
            logger.info("Database backup has been imported Successfully")
            return "Database Restore Successfull"
        else:
            print("Restore Unsuccessfull successfull \n"+str(stderr))
            sys.exit(0)

# dbimportFunc()


